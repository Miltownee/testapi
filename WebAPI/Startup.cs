﻿using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
//using System.Configuration;
using WebAPI.Data;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration
        {
            get;
        }




        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddRazorPages();
            //services.AddDbContext<CustomerDbContext>(opt =>
            //{
            //    opt.EnableSensitiveDataLogging();
            //    opt.UseSqlServer(Configuration.GetConnectionString("AuthConnectionString"));
            //});
            services.AddDbContext<CustomerDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("AuthConnectionString")));
            services.AddControllers();
            //services.AddMvc();
            services.AddSwaggerGen(c => 
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "CustomerAPI",
                Version = "v1",
                Contact = new OpenApiContact()
                {
                    Name = "tim",
                    Email = "tim.jonasson@homtial.com"
                }
            })
            );
            services.AddAutoMapper(typeof(Startup));
        }

          
        public void Configure(WebApplication app, IWebHostEnvironment env)
        {
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }



            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }



            app.MapControllers();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();
            app.Run();
        }
    }
}
