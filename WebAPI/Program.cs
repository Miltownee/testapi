using WebAPI;
using WebAPI.Controllers;
using WebAPI.Data;
using WebAPI.Models;




//Seeder.Seed();


var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var app = builder.Build();




var startup = new Startup(builder.Configuration);
startup.Configure(app, builder.Environment);
startup.ConfigureServices(builder.Services);


// Add services to the container.






// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

//app.UseHttpsRedirection();

//app.UseAuthorization();

//app.MapControllers();

//app.Run();
