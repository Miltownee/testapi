﻿using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class CustomerDbContext : DbContext
    {

        public CustomerDbContext(DbContextOptions options) : base(options)
        {

        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasData(
                new Customer()
                {
                    Name = "Emil",
                    CustomerId = 5
                });
            modelBuilder.Entity<Customer>().HasData(
                new Customer() { Name = "Sanjin", CustomerId = 2 }
                );
            modelBuilder.Entity<Customer>().HasData(
                new Customer() { Name = "Tilda", CustomerId = 2 }
                );
        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Rating> Rating { get; set; }

        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }
        public DbSet<Movies> Movies { get; set; }
    }
}
