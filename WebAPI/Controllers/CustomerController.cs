﻿using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerDbContext _context;


        public CustomerController(CustomerDbContext _context)
        {
            _context = _context;
        }





        [HttpGet("{id}")]
        public async Task<ActionResult<Character>> CharacterById(int id)
        {
            var character = await _context.Character.FindAsync(id);

            return Ok(character);
        }



        //[HttpGet]
        //public ActionResult<Customer> CustomerGet()
        //{
        //    return new Customer() { Name = "Josh", CustomerId = 1 };
        //}

        //[HttpPost]
        //public ActionResult<string> AddCustomer(Customer customer)
        //{
        //    bool isValid = true;
        //    if (isValid)
        //    {
        //        return BadRequest();
        //    }
        //    return CreatedAtAction("GetById", new { CustomerId = customer.CustomerId }, customer);
        //}


        //// Get: api/<CustomerController>
        //[HttpGet("all")]
        //public async Task<ActionResult<IEnumerable<Customer>>> Get()
        //{

        //    // this sould return a list of all customers from the database

        //    List<Customer> customers = new List<Customer>() { new Customer() {
        //        Name = "Emil",
        //        CustomerId = 1
        //    },
        //    new Customer() { Name = "Sanjin", CustomerId = 2 }
        //    //new Customer() { Name = "Tilda", CustomerId = 3} // not here 
        //    };
        //    return Ok(customers);
        //}



        //[HttpGet("{id}")]
        //public async Task<ActionResult<Customer>> GetById(int id)
        //{
        //    var customer = await _context.Customer.FindAsync(id);

        //    return Ok(customer);
        //}

        //[HttpPost]
        //public ActionResult Post([FromBody] Customer customer)
        //{
        //    _context.Customer.Add(customer);

        //    _context.SaveChanges();

        //    return Ok();
        //}

        //[HttpPut("{id}")]
        //public ActionResult Put(int id, [FromBody] Customer updatedCustomer)
        //{
        //    if (id != updatedCustomer.CustomerId)
        //    {
        //        return BadRequest();
        //    }

        //    try
        //    {

        //        _context.Entry(updatedCustomer).State = EntityState.Modified;

        //        _context.SaveChanges();

        //    }
        //    catch
        //    {

        //        return StatusCode(StatusCodes.Status500InternalServerError); // enum with specific errors
        //    }

        //    return NoContent();
        //}

    }
}
