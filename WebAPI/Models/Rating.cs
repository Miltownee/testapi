﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Rating
    {
        public int RatingId { get; set; }
        [Range(0,100, ErrorMessage = "Score is between 0 -> 100")]
        public int Score { get; set; }
        [MaxLength(200)]
        public string Comment { get; set; }

        public int CustomerId { get; set; }

        public Customer customer { get; set; } // One -> Many
    }
}
