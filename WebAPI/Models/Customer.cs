﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        [MaxLength(50)]
        public string Name {get;set;}

        public ICollection<Rating> Ratings { get; set; } // Many -> One
    }
}
